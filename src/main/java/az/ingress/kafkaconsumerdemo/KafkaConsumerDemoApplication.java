package az.ingress.kafkaconsumerdemo;

import az.ingress.kafkaconsumerdemo.domain.User;
import az.ingress.kafkaconsumerdemo.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.DltHandler;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.retry.annotation.Backoff;

@SpringBootApplication
@EnableKafka
@RequiredArgsConstructor
@Slf4j
public class KafkaConsumerDemoApplication {
    private final UserRepository userRepository;
    private final ObjectMapper objectMapper;

    public static void main(String[] args) {
        SpringApplication.run(KafkaConsumerDemoApplication.class, args);
    }

    @RetryableTopic(attempts = "4",backoff = @Backoff(delay = 5000L,multiplier = 3.0))
    @KafkaListener(topics = "ms-demo")
    public void listener(ConsumerRecord<String,String> dto) throws JsonProcessingException {
        log.info("message received {}",dto);
        log.info("message value {}",dto.value());

        var a = objectMapper.convertValue(dto.value(), StudentDto.class);
        log.info(" a"+a);
        log.info("a.getEmail()  "+a.getEmail());
        log.info("a.name()  "+a.getName().toString());
    }
    @DltHandler
    public void deadLetterListener(ConsumerRecord<String,String> dto){
        log.info("deadletter working");
        log.info("deadletter == {}",dto);
        log.info("deadletter value {} ",dto.value().replace("\\","").replace("\"\"\"\"",""));
        log.info("deadletter topic {} ",dto.topic());

    }

        @KafkaListener(topics = "teacher-demo")
    public void teacherListener(ConsumerRecord<String,TeacherDto> message){
        log.info("message received {}",message);
        log.info("message received {}",message.value());
    }

    @SneakyThrows
    @KafkaListener(topics = "security-register")
    public void securityListener(ConsumerRecord<String,UserDto> message){

        log.info("message received from security {}",message);
        log.info("message received from security {}",message.value());
        ObjectMapper mapper= new ObjectMapper();
        UserDto dto = mapper.readValue(message.value().toString(), UserDto.class);
        log.info("userDto {}",dto);
//        UserDto dto = message.value();
//        User save = userRepository.save(User.builder()
//                .id(dto.getId())
//                .username(dto.getUsername())
//                .password(dto.getPassword())
//                .isAccountNonExpired(dto.isAccountNonExpired())
//                .isCredentialsNonExpired(dto.isCredentialsNonExpired())
//                .isEnabled(dto.isEnabled()).build());
//        log.info("user saved {}",save);
    }
}



//    @SneakyThrows
//    @KafkaListener(topics = "ms-demo",containerFactory = "jsonKafkaListenerContainerFactory")
//    public void listener(ConsumerRecord<String,String> message){
//        StudentDto studentDto = mapper.readValue(message.value(), StudentDto.class);
//        log.info("studentDto {}",studentDto);
//    }
