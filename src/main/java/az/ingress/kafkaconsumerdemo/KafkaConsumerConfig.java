//package az.ingress.kafkaconsumerdemo;
//
//import lombok.RequiredArgsConstructor;
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import org.apache.kafka.common.serialization.StringSerializer;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.annotation.EnableKafka;
//import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
//import org.springframework.kafka.config.KafkaListenerContainerFactory;
//import org.springframework.kafka.core.*;
//import org.springframework.kafka.listener.ErrorHandler;
//import org.springframework.kafka.support.serializer.JsonDeserializer;
//import org.springframework.kafka.support.serializer.JsonSerializer;
//import org.springframework.retry.RetryPolicy;
//import org.springframework.retry.backoff.FixedBackOffPolicy;
//import org.springframework.retry.policy.SimpleRetryPolicy;
//import org.springframework.retry.support.RetryTemplate;
//import org.springframework.util.backoff.FixedBackOff;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//@RequiredArgsConstructor
//@EnableKafka
//public class KafkaConsumerConfig {
//    private final KafkaErrorHandler kafkaErrorHandler;
//
//    @Value("${spring.kafka.consumer.bootstrap-servers}")
//    private String bootstrapServer;
//    @Value("${spring.kafka.consumer.group-id}")
//    private String groupId;
//    @Value("${spring.kafka.consumer.properties.back.off.period}")
//    private Long backOffPeriod;
//
//
//    private Map<String,Object> config() {
//        Map<String, Object> props= new HashMap<>();
//        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServer);
//        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
//        return props;
//    }
//    public ConsumerFactory<String,String> consumerFactory(){
//        return new DefaultKafkaConsumerFactory<>(config());
////        return new DefaultKafkaConsumerFactory<>(config(),new StringDeserializer(),deserializer());
//    }
//
//    @Bean
//    public KafkaListenerContainerFactory<?> kafkaListenerContainerFactory(){
//        var factory= new ConcurrentKafkaListenerContainerFactory<String,String>();
//        factory.setConsumerFactory(consumerFactory());
////        factory.setRetryTemplate(retryTemplate());
////        factory.setErrorHandler(kafkaErrorHandler);
////        factory.setRecordFilterStrategy(rec ->rec.value().contains("IGNORED") );
//        return factory;
//    }
//
//    @Bean
//    public RetryTemplate retryTemplate(){
//        final RetryTemplate retryTemplate = new RetryTemplate();
//        final FixedBackOffPolicy backOffPolicy= new FixedBackOffPolicy();
//        backOffPolicy.setBackOffPeriod(backOffPeriod);
//        retryTemplate.setBackOffPolicy(backOffPolicy);
//        retryTemplate.setRetryPolicy(getSimpleRetryPolicy());
//        return retryTemplate;
//    }
//
//    private SimpleRetryPolicy getSimpleRetryPolicy() {
//        final HashMap<Class<? extends Throwable>,Boolean> errorMap= new HashMap<>();
//        errorMap.put(NullPointerException.class,true);
//        //default value will retry or not outside of error map
//        return new SimpleRetryPolicy(3,errorMap,true,true);
//    }
////    public JsonDeserializer<Object> deserializer(){
////        JsonDeserializer<Object> deserializer= new JsonDeserializer<>(Object.class);
////        deserializer.setRemoveTypeHeaders(false);
////        deserializer.addTrustedPackages("*");
////        deserializer.setUseTypeMapperForKey(true);
////        return deserializer;
////    }
////    @Bean
////    public KafkaListenerContainerFactory<?> jsonKafkaListenerContainerFactory(){
////        var factory= new ConcurrentKafkaListenerContainerFactory<String,String>();
////        factory.setConsumerFactory(customConsumerFactory());
////        return factory;
////    }
////    public ConsumerFactory<String,Object> customConsumerFactory(){
////        Map<String, Object> props= new HashMap<>();
////        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,bootstrapServer);
////        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
////        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
////        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
////        return new DefaultKafkaConsumerFactory<>(props);
////    }
//
//}
