package az.ingress.kafkaconsumerdemo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {
    Long id;
    String name;
    String lastName;
    String email;
    Integer age;
}
