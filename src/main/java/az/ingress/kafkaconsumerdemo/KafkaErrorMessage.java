package az.ingress.kafkaconsumerdemo;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KafkaErrorMessage<T> {
    private String error;
    private T data;
}
